package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;
import java.util.List;


public class ChainSpell implements Spell {
    List<Spell> listSpell;

    public ChainSpell(ArrayList<Spell> list){
        this.listSpell= list;
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }

    @Override
    public void cast(){
        for(int i = 0; i<listSpell.size(); i++){
            listSpell.get(i).cast();
        }
    }

    @Override
    public void undo(){
        for(int n = 0; n< listSpell.size()-1; n++){
            listSpell.get(n).undo();
        }
    }

}
